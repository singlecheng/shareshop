<%@ page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>商品上架</title>
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- load navibar -->
	<jsp:include page="/navibar.jsp" />
	<div>
		<div class="jumbotron">
			<h1><b><font color ="#FFFFFF">&nbsp&nbsp&nbsp商品上架</font></b></h1><br>
			<p></p>
			<a href="/Fileupload" class="btn btn-primary btn-lg" class="button"><span class="glyphicon glyphicon-plus-sign"></span> 商品上架</a>
			<a href="/Manage/productmanage.jsp" class="btn btn-primary btn-lg" class="button"><span class="glyphicon glyphicon-plus-sign"></span> 商品管理</a>
			<a href="/Manage/ordermanage.jsp" class="btn btn-primary btn-lg" class="button"><span class="glyphicon glyphicon-list-alt"></span> 訂單管理</a>
			<a href="/Manage/infoedit.jsp" class="btn btn-warning btn-lg" class="button"><span class="glyphicon glyphicon-plus-sign"></span> 修改會員資料</a>		
		</div>
	</div>

	<div class="container">
		<form id="form1" method="post" action="fileupload.jsp">
			<div class="form-group">
				<label for="name">商品名稱:</label> <input type="text" 
					class="form-control" id="name" name="name" value="請稍後命名~~" disabled>
			</div>
			<div class="form-group">
				<label for="price">售 價:</label> <input type="text"
					class="form-control" id="price" name="price" value="請稍後命名~~" disabled>
			</div>
			<div class="form-group">
				<label for="contract">合約期限:</label> <input type="text"
					class="form-control" id="contract" name="contract" value="請稍後命名~~" disabled>
			</div>
		</form>

		<form id="form2" method="post" action="fileupload.jsp"
			enctype="multipart/form-data">
			<b>選擇商品圖片上傳:</b> <br />
			<div>
				<input type="file" class="btn btn-success" required name="file" size="50"><br />
			</div>
		</form>

		<script type="text/javascript">
			submitForms = function() {
				document.getElementById("form1").submit();
				document.getElementById("form2").submit();
			}
		</script>
		<input type="button" class="btn btn-danger" value="確認上架"
			onclick="submitForms()" />
	</div>
</body>
</html>
