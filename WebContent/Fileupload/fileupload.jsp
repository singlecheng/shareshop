<%@ page import="java.io.*,java.util.*, javax.servlet.*, java.sql.*"%>
<%@ page import="javax.servlet.http.*"%>
<%@ page import="org.apache.commons.fileupload.*"%>
<%@ page import="org.apache.commons.fileupload.disk.*"%>
<%@ page import="org.apache.commons.fileupload.servlet.*"%>
<%@ page import="org.apache.commons.io.output.*"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%
	request.getRemoteAddr();

	File file;
	int maxFileSize = 5000 * 1024;
	int maxMemSize = 5000 * 1024;
	ServletContext context = pageContext.getServletContext();
	String filePath = context.getInitParameter("file-upload");

	// Verify the content type
	String contentType = request.getContentType();
	if ((contentType.indexOf("multipart/form-data") >= 0))
	{

		DiskFileItemFactory factory = new DiskFileItemFactory();
		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);
		// Location to save data that is larger than maxMemSize.
		factory.setRepository(new File("c:\\temp"));

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);
		try
		{
			// Parse the request to get file items.
			List fileItems = upload.parseRequest(request);

			// Process the uploaded file items
			Iterator i = fileItems.iterator();

			out.println("<html>");
			out.println("<head>");
			out.println("<title>JSP File upload</title>");
			out.println("</head>");
			out.println("<body>");
			while (i.hasNext())
			{
				FileItem fi = (FileItem) i.next();
				if (!fi.isFormField())
				{
					// Get the uploaded file parameters
					String fieldName = fi.getFieldName();
					String fileName = fi.getName();
					boolean isInMemory = fi.isInMemory();
					long sizeInBytes = fi.getSize();
					// Write the file
					if (fileName.lastIndexOf("\\") >= 0)
					{
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
					} else
					{
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}
					fi.write(file);
					//insert log 
					Class.forName("com.mysql.jdbc.Driver");
					String url = "jdbc:mysql://localhost/?useUnicode=true&characterEncoding=utf8";
					String sql = "";
					Connection con = DriverManager.getConnection(url, "root", "");
					sql = "use jsp";
					con.createStatement().execute(sql);
					request.setCharacterEncoding("UTF-8");//UTF-8

					sql = "INSERT INTO `product` (`id`, `name`, `price`, `contract`, `imgpath`, `time`, `brand`, `stock`, `memberid`) VALUES (NULL, '尚未完成命名', '尚未定價', '尚未訂約', '"
							+ fileName + "', CURRENT_TIMESTAMP, '品牌未定', '尚未填寫庫存', '尚未完成編輯,尚無ID')";
					con.createStatement().execute(sql);
					con.close();
					response.sendRedirect("/Manage/productmanage.jsp");
				}
			}
			out.println("</body>");
			out.println("</html>");
		} catch (Exception ex)
		{
			System.out.println(ex);
		}
	} else
	{
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Servlet upload</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<p>No file uploaded</p>");
		out.println("</body>");
		out.println("</html>");
	}
%>