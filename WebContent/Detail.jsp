<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>訂單確認</title>
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
<!-- AngulaJS -->
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
</head>
<body>
	<!-- load navibar -->
	<jsp:include page="/navibar.jsp" />

	<div class="jumbotron">

		<h1>
			<b><font color="#FFFFFF">&nbsp&nbsp&nbsp確認購買資訊</font></b>
		</h1>
		<br>
	</div>


	<%
		String pid = request.getParameter("pid");
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			String url = "jdbc:mysql://localhost:3306/jsp";
			String user = "root";
			String password = "";
			String driver = "com.mysql.jdbc.Driver";
			Class.forName(driver);
			con = DriverManager.getConnection(url, user, password);
			stmt = con.createStatement();
			String sql = "SELECT * FROM `product` WHERE id = '" + pid + "'";
			rs = stmt.executeQuery(sql);
		} catch (Exception ex)
		{
			System.out.println(ex);
		}

		if (rs != null)
		{
			while (rs.next())
			{
				String id = rs.getString(1);
				String name = rs.getString(2);
				String price = rs.getString(3);
				String contract = rs.getString(4);
				String imgpath = rs.getString(5);
				String time = rs.getString(6);
				String stock = rs.getString(8);
	%>

	<div class="text-center">
		<hr />
		<tr>
			<td><form action="/Purchase/purchasecom.jsp" method="post">
					<!-- load img -->
					<p>
						<img alt="" src="/data/<%=imgpath%>"
							style="width: 304px; height: 228px;">
					</p>

					<!-- hidden, input memberID -->
					<div class="form-group" style="visibility: hidden">
						<label class="control-label col-md-2">Name</label>
						<%
							String sql = "SELECT * FROM `members` WHERE `uname` = '" + session.getAttribute("userid") + "'";
									rs = stmt.executeQuery(sql);
									if (rs != null)
									{
										while (rs.next())
										{
											String memberid = rs.getString(1);
						%><div class="col-md-10">
							<input class="form-control text-box single-line" id="name"
								name="pmemberid" type="text" value="<%=memberid%>"> <span
								class="field-validation-valid text-danger"
								data-valmsg-for="SalePrice" data-valmsg-replace="true"></span>
						</div>
						<%
							}
									}
						%><br />
					</div>

					<!-- load productID&Name -->
					<div>
						<p>
							商品名稱 :
							<%=name%><br />
						<p hidden>
							<input type="text" name="pname" value="<%=name%>">
						</p>
						<p hidden>
							<input type="text" name="pid" value="<%=id%>">
						</p>
						</p>
					</div>
					<div>
						<p>
							價 格 :
							<%=price%><br />

						</p>
					</div>
					<div>
						<p>
							合 約 :
							<%=contract%><br />
						<p hidden>
							<input type="text" name="pcontract" value="<%=contract%>">
						</p>
						</p>
					</div>
					<!-- load online time -->
					<div>
						<p>
							上架時間 :
							<%=time%><br />
						</p>
					</div>
					<div>
						<p>
							庫存數量 :
							<%=stock%><br />
						</p>
					</div>
					<div data-ng-app="" data-ng-init="quantity=1; price=<%=price%>">
					<p>
							購買數量 : <input type="number" ng-model="quantity" min="1"
								max="<%=stock%>">&nbsp&nbsp&nbsp&nbsp 單 價 : <input
								type="number" ng-model="price" disabled>&nbsp&nbsp&nbsp&nbsp
							<b>合計金額 : {{quantity * price}} 元</b>
						<p hidden>
							<input type="text" name="pprice" value="{{quantity * price}}">
						</p>
						<p hidden>
							<input type="text" name="quantity" value="{{quantity}}">
						</p>
						</p>

					</div>

					<br><form><div class="col-md-offset-4 col-md-4">
                    <fieldset class="form-group">
							收件人地址 : <input class="form-control text-box single-line"
								id="Context" name="address" type=text placeholder="請輸入收件人地址" required />
					</fieldset>

					 <fieldset class="form-group">
								付款方式 : <select class="form-control" name="pay" required>
									<option>ibon繳費</option>
									<option>7-11、全家、萊爾富取貨付款</option>
									<option>銀行或郵局轉帳</option>
									<option>郵局無摺存款</option>
								</select>
					</fieldset>
					<br> <input type="submit" class="btn btn-danger" value="確認送出">
				</div></form><br>

		<%
			}
			}
		%>
	</div>
			<br><br><br><br><br><br><br><br><br><br><br><br><footer>
						<ul class="actions">
							<li>
								<a href="#" class="button alt big"><center>TOP</center></a>
							</li>
						</ul>
						</footer>

	</div>
	<footer class="container-fluid bg-4 text-center" style="background-color:#00CACA">
		<p><br>
			<font color ="#FFFFFF">Powered By : </font><a href="https://www.android.com/">Android</a> | <a
				href="https://angularjs.org/">AngularJS</a> | <a
				href="https://bitbucket.org/">Bitbucket</a> | <a
				href="http://getbootstrap.com/">Bootstrap</a> | <a
				href="http://codeigniter.org.tw/">CodeIgniter</a> | <a
				href="https://eclipse.org/downloads/">Eclipse</a> | <a
				href="http://estimote.com/">Estimote</a> | <a
				href="https://github.com/">GitHub</a> | <a
				href="https://developer.apple.com/ibeacon/">iBeacon</a> | <a
				href="https://jquery.com/">jQuery</a> | <a
				href="http://www.json.org/">JSON</a> | <a
				href="https://www.mysql.com/downloads/">MySQL</a> | <a
				href="http://php.net/downloads.php">PHP</a> | <a
				href="https://www.sourcetreeapp.com/">SourceTree</a> | <a
				href="https://developer.apple.com/swift/">Swift</a> | <a
				href="https://www.apachefriends.org/zh_tw/index.html">Xampp</a> |
		</p>
		<p class="footer_copyright">
			<font color ="#FFFFFF">© 2016 </font><a href="mailto:singleqiang@gmail.com">Single Studio</a>. 
			<font color ="#FFFFFF">All Rights Reserved.</font>
		</p>

		<!-- 訪客計數器 -->
		<%
			Integer hitsCount = (Integer) application.getAttribute("hitCounter");
			if (hitsCount == null || hitsCount == 0)
			{
				/* First visit */
				out.println("Welcome !");
				hitsCount = 1;
			} else
			{
				/* return visit */
				out.println("Welcome back !");
				hitsCount += 1;
			}
			application.setAttribute("hitCounter", hitsCount);
		%>

		<p><font color ="#FFFFFF">
			累計拜訪人數 : 
			<%=hitsCount%> 人</font></p>

	</footer>
</body>
</html>