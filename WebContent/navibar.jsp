<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*, java.util.*"%>
<!-- navi bar -->
<style>
.navbar-nav  li a:hover {
	color: #FFAA33 !important;
}

.jumbotron {
	background-color: #00AEAE;
	color: #FFFFFF;
	padding: 1px 100px;
	font-family: Montserrat, sans-serif;
}
</style>

<nav class="navbar-primary ​navbar-fixed-top"  style="background-color:#00CACA">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/index.jsp"><font color="#FFFFFF" size="6">ShareShop</font></a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li class="active"><a href="/index.jsp"><span class="glyphicon glyphicon-home" style='color:#FFFFFF;'></span><font color="#FFFFFF"><strong>&nbsp&nbsp&nbspHome</strong></font></a></li>
				<li class="active"><a href="/search"><span class="glyphicon glyphicon-search" style='color:#FFFFFF;'></span><font color="#FFFFFF"><strong>&nbsp&nbsp&nbsp商品搜尋</strong></font></a></li>
				<!-- <li><a href=""><span class="glyphicon glyphicon-tags"></span>
						Promotion</a></li> -->
				<!-- <li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"><span
						class="glyphicon glyphicon-th-list"></span> Brand<span
						class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="">Apple</a></li>
						<li><a href="">ASUS</a></li>
						<li><a href="">hTC</a></li>
						<li><a href="">SONY</a></li>
						<li><a href="">SAMSUNG</a></li>
						<li><a href="">Microsoft</a></li>
						<li><a href="">Google</a></li>
						<li><a href="">Others</a></li>
					</ul></li> -->
				<li class="active"><a href="/BusinessCart"><span class="glyphicon glyphicon-briefcase" style='color:#FFFFFF;'></span><font color="#FFFFFF"><strong>&nbsp&nbsp&nbsp企業採購</strong></font></a></li>
				<li class="active"><a href="/Board"><span class="glyphicon glyphicon-bullhorn" style='color:#FFFFFF;'></span><font color="#FFFFFF"><strong>&nbsp&nbsp&nbsp留言板</strong></font></a></li>
				<li class="active"><a href="/CC"><span class="glyphicon glyphicon-tree-deciduous" style='color:#FFFFFF;'></span><font color="#FFFFFF"><strong>&nbsp&nbsp&nbsp關於我們</strong></font></a></li>

			</ul>

			<ul class="nav navbar-nav navbar-right">
				<%
					if (session.getAttribute("userid") == null)
					{
						out.print("<li><a href='/Account'><span class='glyphicon glyphicon-log-in' style='color:#FFFFFF;'> <b>登 入</b></span></a></li>");
						out.print("<li><a href='/Account/reg.jsp'><span class='glyphicon glyphicon-plus-sign' style='color:#FFFFFF;'> <b>註 冊</b></span></a></li>");
						
					}
					if (session.getAttribute("userid") != null)
					{
						out.print("<li><a href='/Order'><span class='glyphicon glyphicon-shopping-cart' style='color:#FFFFFF;'> <b>我的訂單</b></span></a></li>");
						out.print("<li><a href='/Manage'><span style='color:#FFFFFF;'><b>您好，</b></span> ");
						out.print(session.getAttribute("userid"));
						out.print("</a></li>");
						out.print("<li><a href='/Account/logout.jsp'><span class='glyphicon glyphicon-log-in' style='color:#FFFFFF;'> <b>登出</b></span></a></li>");
					}
				%>
				
			</ul>
		</div>
	</div>
</nav>