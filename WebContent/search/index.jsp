<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.sql.*, java.util.*"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>商品搜尋</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="/BeaconOrderServer/assets/image/favicon.ico"
	rel="SHORTCUT ICON">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
<script
	src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0-rc.0/angular.min.js"></script>
</head>

<body ng-app="">

	<jsp:include page="/navibar.jsp" />

	<div class="jumbotron">
		<h1>
			<b><font color="#FFFFFF">&nbsp&nbsp&nbsp商品搜尋</font></b>
		</h1>
		<br>
	</div>

	<%
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		String url = "jdbc:mysql://localhost:3306/jsp";
		String user = "root";
		String password = "";
		String driver = "com.mysql.jdbc.Driver";
		Class.forName(driver);
		con = DriverManager.getConnection(url, user, password);
		stmt = con.createStatement();
		String sql = "SELECT * FROM `product`";
		rs = stmt.executeQuery(sql);
	%>
	<div
		ng-init="friends = [
	
		<%if (rs != null)
			{
				while (rs.next())
				{
					String id = rs.getString(1);
					String name = rs.getString(2);
					String imgpath = rs.getString(5);
					out.print("{id:'" + id + "',name:'" + name + "', img:'" + imgpath + "'},");
				}
			}%>
                         ]"></div>

	<div class="container text-center">
		<label>Search : <input ng-model="searchText"></label>
		<div class="col-md-offset-4 col-md-8">
			<fieldset class="form-group">
				<table id="searchTextResults">
					<tr>
						<th>&nbsp&nbsp&nbsp&nbsp</th>
						<th>商品名稱&nbsp&nbsp&nbsp&nbsp</th>
					</tr>
					<tr ng-repeat="friend in friends | filter:searchText">
						<td><a href="/Detail.jsp?pid={{friend.id}}"><img alt=""
								src="/data/{{friend.img}}" style="width: 280px; height: 200px;"></a></td>
						<td>{{friend.name}}</td>
					</tr>
				</table>
			</fieldset>
		</div>
	</div>
</body>
</html>