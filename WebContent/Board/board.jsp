<%@ page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
<title>留言版</title>
</head>
<body>
	<!-- load navibar -->
	<jsp:include page="/navibar.jsp" />
	
	<div class="">
		<div class="jumbotron">
			<h1><b><font color ="#FFFFFF">&nbsp&nbsp&nbsp訪客留言板</font></b></h1><br>
			<p>
				&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a class="btn btn-primary" href="index.jsp?page=1">觀看留言</a>
			</p>
		</div>
	</div>

	<div class="container">
		<b><h2>我想說的話</h2></b>
		<font color="red">*</font> 為必填欄位。
		<form name="form1" method="post" action="add.jsp">
			<div class="form-group">
				<p hidden>
					IP：<input name="ip" value='<%out.print(request.getRemoteAddr());%>'><br>
				</p>
			</div>

			<div class="form-group">
				<label for="name">訪客暱稱:</label> <input type="text"
					class="form-control" id="name" name="name">
			</div>
			<div class="form-group">
				<label for="pwd">E-Mail:</label> <input type="text"
					class="form-control" id="mail" name="mail">
			</div>
			<div class="form-group">
				<label for="subject">主 旨<font color="red">*</font>:</label> <input type="text"
					class="form-control" id="subject" name="subject" required>
			</div>
			<div class="form-group">
				<label for="content">留言內容<font color="red">*</font>:</label>
				<textarea class="form-control" rows="5" id="content" name="content" required></textarea>
			</div>

			<div>
				<input type="submit" class="btn btn-info" value="送 出">&nbsp&nbsp
				<input type="Reset" class="btn btn-info" name="Reset" value="清除內容">
			</div>
		</form>
	</div><br><br><br><br>
	<footer class="container-fluid bg-4 text-center" style="background-color:#00CACA">
		<p><br>
			<font color ="#FFFFFF">Powered By : </font><a href="https://www.android.com/">Android</a> | <a
				href="https://angularjs.org/">AngularJS</a> | <a
				href="https://bitbucket.org/">Bitbucket</a> | <a
				href="http://getbootstrap.com/">Bootstrap</a> | <a
				href="http://codeigniter.org.tw/">CodeIgniter</a> | <a
				href="https://eclipse.org/downloads/">Eclipse</a> | <a
				href="http://estimote.com/">Estimote</a> | <a
				href="https://github.com/">GitHub</a> | <a
				href="https://developer.apple.com/ibeacon/">iBeacon</a> | <a
				href="https://jquery.com/">jQuery</a> | <a
				href="http://www.json.org/">JSON</a> | <a
				href="https://www.mysql.com/downloads/">MySQL</a> | <a
				href="http://php.net/downloads.php">PHP</a> | <a
				href="https://www.sourcetreeapp.com/">SourceTree</a> | <a
				href="https://developer.apple.com/swift/">Swift</a> | <a
				href="https://www.apachefriends.org/zh_tw/index.html">Xampp</a> |
		</p>
		<p class="footer_copyright">
			<font color ="#FFFFFF">© 2016 </font><a href="mailto:singleqiang@gmail.com">Single Studio</a>. 
			<font color ="#FFFFFF">All Rights Reserved.</font>
		</p>

		<!-- 訪客計數器 -->
		<%
			Integer hitsCount = (Integer) application.getAttribute("hitCounter");
			if (hitsCount == null || hitsCount == 0)
			{
				/* First visit */
				out.println("Welcome !");
				hitsCount = 1;
			} else
			{
				/* return visit */
				out.println("Welcome back !");
				hitsCount += 1;
			}
			application.setAttribute("hitCounter", hitsCount);
		%>

		<p><font color ="#FFFFFF">
			累計拜訪人數 : 
			<%=hitsCount%> 人</font></p>

	</footer>
</body>
</html>

</body>
</html>
