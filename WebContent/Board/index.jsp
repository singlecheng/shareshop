<!-- Step 0: import library -->
<%@ page import="java.sql.*, java.util.*"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
<head>
<title>Message Board - Share Shop</title>
</head>
<body>
	<!-- load navibar -->
	<jsp:include page="/navibar.jsp" />
	
	<div class="">
		<div class="jumbotron">
			<h1><b><font color ="#FFFFFF">&nbsp&nbsp&nbsp訪客留言板</font></b></h1><br>
			<p>
				&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a class="btn btn-primary" href="board.jsp">我要留言</a>
			</p>
		</div>
	</div>

	<div class="container">

		<table class="table">
			<%
				try
				{
					//Step 1: 載入資料庫驅動程式 
					Class.forName("com.mysql.jdbc.Driver");
					try
					{
						//Step 2: 建立連線 	
						String url = "jdbc:mysql://localhost/";
						String sql = "";
						Connection con = DriverManager.getConnection(url, "root", "");

						if (con.isClosed())
							out.println("連線建立失敗");
						else
						{
							//Step 3: 選擇資料庫   
							sql = "use jsp";
							con.createStatement().execute(sql);

							//Step 4: 執行 SQL 指令, 若要操作記錄集, 需使用executeQuery, 才能傳回ResultSet	
							sql = "select * from guestbook"; //算出共幾筆留言
							ResultSet rs = con.createStatement().executeQuery(sql);

							//先移到檔尾, getRow()後, 就可知道共有幾筆記錄
							rs.last();
							int total_content = rs.getRow();
							out.println("<h2><b>總留言數 : " + total_content + "</b></h2>");

							//每頁顯示5筆, 算出共幾頁
							int page_num = (int) Math.ceil((double) total_content / 5.0); //無條件進位
							if (page_num == 0) //無留言時將頁數指標訂為1
								page_num = 1;
							out.println("頁數選擇");
							//使用超連結方式, 呼叫自己, 使用get方式傳遞參數(變數名稱為page)
							for (int i = 1; i <= page_num; i++) //使用get傳輸方式,建立1,2,...頁超連結
							{
								out.print("<a href='index.jsp?page=" + i + "'>" + i + "</a>&nbsp;"); //&nbsp;html的空白
							}
							out.println("<p>");

							//讀取page變數
							String page_string = request.getParameter("page");
							if (page_string == null)
								page_string = "1"; //無留言時將頁數指標訂為1        
							Integer current_page = Integer.valueOf(page_string);//將page_string轉成整數
							//Integer current_page=Integer.valueOf(request.getParameter("page"));
							//計算開始記錄位置   
							//Step 5: 顯示結果 
							int start_record = (current_page - 1) * 5;
							
							//遞減排序, 讓最新的資料排在最前面
							sql = "SELECT * FROM guestbook ORDER BY no DESC LIMIT ";//LIMIT是限制傳回筆數
							sql += start_record + ",5";
							
							//上述語法解讀如下:
							// current_page... select * from guestbook order by no desc limit
							//      current_page=1: select * from guestbook order by no desc limit 0, 5 //從第0筆顯示5筆
							//      current_page=2: select * from guestbook order by no desc limit 5, 5 //從第5筆顯示5筆
							//      current_page=3: select * from guestbook order by no desc limit 10, 5//從第10筆顯示5筆

							rs = con.createStatement().executeQuery(sql);

							//  逐筆顯示, 直到沒有資料(最多還是5筆)
							while (rs.next())
							{
								//out.println("留言主題:"+rs.getString(4)+"<br>");
								out.println("<tr><td>" + "主旨:" + rs.getString("subject") + "<br>");
								//out.println("訪客姓名:"+rs.getString(2)+"<br>");
								out.println("" + "訪客暱稱:" + rs.getString("name") + "<br>");
								//out.println("E-mail:"+rs.getString(3)+"<br>");
								out.println("" + "E-mail:" + rs.getString("mail") + "<br>");
								//out.println("留言內容:"+rs.getString(5)+"<br>");
								out.println("" + "留言內容:" + "<pre>" + rs.getString("content") + "</pre></br>");
								//IP
								out.println("" + "IP 位址 : " + rs.getString("ip") + "<br>");
								//out.println("留言時間:"+rs.getString(6)+"<br><hr>");
								out.println("" + "留言時間:" + rs.getString("putdate") + "<br></tr>");
							}
							//Step 6: 關閉連線
							con.close();
						}
					} catch (SQLException sExec)
					{
						out.println("SQL錯誤");
					}
				} catch (ClassNotFoundException err)
				{
					out.println("class錯誤");
				}
			%>
		</table>
		
		<footer>
						<ul class="actions">
							<li>
								<a href="#" class="button alt big"><center>TOP</center></a>
							</li>
						</ul>
						</footer>

	</div>
	<footer class="container-fluid bg-4 text-center" style="background-color:#00CACA">
		<p><br>
			<font color ="#FFFFFF">Powered By : </font><a href="https://www.android.com/">Android</a> | <a
				href="https://angularjs.org/">AngularJS</a> | <a
				href="https://bitbucket.org/">Bitbucket</a> | <a
				href="http://getbootstrap.com/">Bootstrap</a> | <a
				href="http://codeigniter.org.tw/">CodeIgniter</a> | <a
				href="https://eclipse.org/downloads/">Eclipse</a> | <a
				href="http://estimote.com/">Estimote</a> | <a
				href="https://github.com/">GitHub</a> | <a
				href="https://developer.apple.com/ibeacon/">iBeacon</a> | <a
				href="https://jquery.com/">jQuery</a> | <a
				href="http://www.json.org/">JSON</a> | <a
				href="https://www.mysql.com/downloads/">MySQL</a> | <a
				href="http://php.net/downloads.php">PHP</a> | <a
				href="https://www.sourcetreeapp.com/">SourceTree</a> | <a
				href="https://developer.apple.com/swift/">Swift</a> | <a
				href="https://www.apachefriends.org/zh_tw/index.html">Xampp</a> |
		</p>
		<p class="footer_copyright">
			<font color ="#FFFFFF">© 2016 </font><a href="mailto:singleqiang@gmail.com">Single Studio</a>. 
			<font color ="#FFFFFF">All Rights Reserved.</font>
		</p>

		<!-- 訪客計數器 -->
		<%
			Integer hitsCount = (Integer) application.getAttribute("hitCounter");
			if (hitsCount == null || hitsCount == 0)
			{
				/* First visit */
				out.println("Welcome !");
				hitsCount = 1;
			} else
			{
				/* return visit */
				out.println("Welcome back !");
				hitsCount += 1;
			}
			application.setAttribute("hitCounter", hitsCount);
		%>

		<p><font color ="#FFFFFF">
			累計拜訪人數 : 
			<%=hitsCount%> 人</font></p>

	</footer>
</body>
</html>
</body>
</html>
