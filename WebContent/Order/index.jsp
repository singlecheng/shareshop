<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>

<%
	if ((session.getAttribute("userid") == null) || (session.getAttribute("userid") == ""))
	{
%>
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
<!-- load navibar -->
<jsp:include page="/navibar.jsp" />
<div align="center">
	Access denied <br /> <a href="/Account">Please Login</a>
</div>
<%
	} else
	{
%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>我的訂單</title>
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- load navibar -->
	<jsp:include page="/navibar.jsp" />

		<div class="jumbotron">
			<h1><b><font color ="#FFFFFF">&nbsp&nbsp&nbsp我的訂單</font></b></h1><br>
		</div>

	<div class="container">
		<table class="table">
			<thead>
				<tr>
					<th>訂單編號</th>
					<th>會員ID</th>
					<th>商品ID</th>
					<th>商品名稱</th>
					<th>購買數量</th>
					<th>總金額</th>
					<th>合約期限</th>
					<th>付款方式</th>
					<th>訂單成立時間</th>
					<th>Purchase</th>
				</tr>
			</thead>
			<%
				//Get account ID
					request.getRemoteAddr();
					request.setCharacterEncoding("UTF-8");
					
					String accountid = null;
					Statement stmt = null;
					Connection con = null;
					ResultSet rs = null;
					String url = "jdbc:mysql://localhost:3306/jsp";
					String user = "root";
					String password = "";
					String driver = "com.mysql.jdbc.Driver";
					Class.forName(driver);
					con = DriverManager.getConnection(url, user, password);
					stmt = con.createStatement();
					String sql = "SELECT * FROM `members` WHERE `uname` = '" + session.getAttribute("userid") + "'";
					rs = stmt.executeQuery(sql);
					if (rs != null)
					{
						while (rs.next())
						{
							accountid = rs.getString(1);
						}
					}
			%>
			<tbody>
				<%
					//Get order ID

						sql = "SELECT * FROM `orderlist` WHERE `memberid` = '" + accountid + "' ORDER BY `time` DESC";
						rs = stmt.executeQuery(sql);

						if (rs != null)
						{
							while (rs.next())
							{
								String id = rs.getString(1);
								String memberid = rs.getString(2);
								String pid = rs.getString(3);
								String pname = rs.getString(4);
								String quantity = rs.getString(5);
								String price = rs.getString(6);
								String contract = rs.getString(7);
								String pay = rs.getString(8);
								String time = rs.getString(9);
								String perchase = rs.getString(10);
				%>
				<tr>
					<td><%=id%></td>
					<td><%=memberid%></td>
					<td><%=pid%></td>
					<td><%=pname%></td>
					<td><%=quantity%></td>
					<td><%=price%></td>
					<td><%=contract%></td>
					<td><%=pay%></td>
					<td><%=time%></td>
					<td><%=perchase%></td>
				</tr>
				<%
					}
						}
				%>
			</tbody>
		</table>
	</div>
</body>
</html>

<%
	}
%>