<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%
	if ((session.getAttribute("userid") == null) || (session.getAttribute("userid") == ""))
	{
%>
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
<!-- load navibar -->
<jsp:include page="/navibar.jsp" /><br><br>
<div align="center">
	<b>您尚未登入帳號? <a href="/Account"> 請點此登入</a>。</b>
</div>
<%
	} else
	{
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>企業採購</title>
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/foundation/5.4.0/css/normalize.css" />
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/foundation/5.4.0/css/foundation.css" />
<link rel="stylesheet" href="style.css" />
</head>

<body ng-app="petShopApp" ng-controller="PetShopCtrl">
	<!-- load navibar -->
	<jsp:include page="/navibar.jsp" />
	
		<div class="jumbotron">
			<h1><b><font color ="#FFFFFF">&nbsp&nbsp&nbsp商品瀏覽</font></b></h1><br>
		</div>

	<ul class="product-list small-block-grid-3">
		<li ng-repeat="product in products">
			<figure>
				<img ng-src="/data/{{product.image}}" alt="{{product.title}}">
				<figcaption>
					<h3>{{product.title}}, {{product.price}}$</h3>
					<button ng-click="addToCart(product);">加入購物車</button>
				</figcaption>
			</figure>
		</li>
	</ul>

	<div ng-if="cart.length" class="shopping-cart">
		<h2>{{cart.length}} items in cart, {{getCartPrice()}}$</h2>

		<form action="checkout.jsp">
			<input type="text" value="{{getCartPrice()}}" name="getCartPrice">
			
			<span class="cart-item th radius" ng-repeat="product in cart">
				<img ng-src="/data/{{product.image}}" title="{{product.title}}" />
				<span class="badge" ng-show="product.quantity > 1">
					x{{product.quantity}} </span>
			</span> <input type="submit" class="btn btn-info" value="下一步">
		</form>
	</div>

	<!-- Angular, stripe -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-rc.0/angular.min.js"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-rc.0/angular-animate.min.js"></script>
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

	<!-- angular-payments library - you probably want to install it through either bower or npm -->
	<script
		src="http://cdn.rawgit.com/laurihy/angular-payments/2472bc9befa256780d106a8e53a9dea12b7341ed/lib/angular-payments.js"></script>

	<!-- other angular.js modules -->
	<script
		src="http://pineconellc.github.io/angular-foundation/mm-foundation-tpls-0.3.1.js"></script>
	<script
		src="http://cdnjs.cloudflare.com/ajax/libs/spin.js/2.0.1/spin.js"></script>
	<script
		src="http://cdnjs.cloudflare.com/ajax/libs/angular-spinner/0.5.1/angular-spinner.js"></script>

	<!-- our code -->
	<script src="scripts.js"></script>
	
	
	
		<footer>
						<ul class="actions">
							<li>
								<a href="#" class="button alt big"><center>TOP</center></a>
							</li>
						</ul>
						</footer>

	</div><br>
	<footer class="container-fluid bg-4 text-center" style="background-color:#00CACA">
		<p><br>
			<font color ="#FFFFFF">Powered By : </font><a href="https://www.android.com/">Android</a> | <a
				href="https://angularjs.org/">AngularJS</a> | <a
				href="https://bitbucket.org/">Bitbucket</a> | <a
				href="http://getbootstrap.com/">Bootstrap</a> | <a
				href="http://codeigniter.org.tw/">CodeIgniter</a> | <a
				href="https://eclipse.org/downloads/">Eclipse</a> | <a
				href="http://estimote.com/">Estimote</a> | <a
				href="https://github.com/">GitHub</a> | <a
				href="https://developer.apple.com/ibeacon/">iBeacon</a> | <a
				href="https://jquery.com/">jQuery</a> | <a
				href="http://www.json.org/">JSON</a> | <a
				href="https://www.mysql.com/downloads/">MySQL</a> | <a
				href="http://php.net/downloads.php">PHP</a> | <a
				href="https://www.sourcetreeapp.com/">SourceTree</a> | <a
				href="https://developer.apple.com/swift/">Swift</a> | <a
				href="https://www.apachefriends.org/zh_tw/index.html">Xampp</a> |
		</p>
		<p class="footer_copyright">
			<font color ="#FFFFFF">© 2016 </font><a href="mailto:singleqiang@gmail.com">Single Studio</a>. 
			<font color ="#FFFFFF">All Rights Reserved.</font>
		</p>

		<!-- 訪客計數器 -->
		<%
			Integer hitsCount = (Integer) application.getAttribute("hitCounter");
			if (hitsCount == null || hitsCount == 0)
			{
				/* First visit */
				out.println("Welcome !");
				hitsCount = 1;
			} else
			{
				/* return visit */
				out.println("Welcome back !");
				hitsCount += 1;
			}
			application.setAttribute("hitCounter", hitsCount);
		%>

		<p><font color ="#FFFFFF">
			累計拜訪人數 : 
			<%=hitsCount%> 人</font></p>

	</footer>	
</body>
</html>

<%
	}
%>