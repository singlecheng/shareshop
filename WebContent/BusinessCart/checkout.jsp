<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.sql.*, java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Business Shop</title>
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/foundation/5.4.0/css/normalize.css" />
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/foundation/5.4.0/css/foundation.css" />
<link rel="stylesheet" href="style.css" />

<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
</head>
<body>
	<!-- load navibar -->
	<jsp:include page="/navibar.jsp" />

	<div class="jumbotron">

		<h1>
			<b><font color="#FFFFFF">&nbsp&nbsp&nbsp確認購買資訊</font></b>
		</h1>
		<br>
	</div>

	<form method="post" action="/Purchase/purchasecom.jsp"
		name="checkoutForm" stripe-form="stripeCallback"
		ng-submit="onSubmit()" data-abide>

		<div ng-if="processing" us-spinner class="col-md-offset-4 col-md-4">
			<fieldset>
				<legend>購買明細</legend>

				<div class="row">
					<script
						src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
					<body>
						<div ng-app="myApp" ng-controller="formCtrl">
							購買"種類"數量:<input type="number" ng-model="user.p1" min="1" max="10">
							購買"總"數量:<input type="number" ng-model="user.p2" min="1" max="50" name="quantity">
							選擇隸屬</select> <select class="form-control" ng-model="user.p3">
								<option selected="selected" ng-model="lastName">資管企業</option>
								<option ng-model="p3">醫工企業</option>
								<option ng-model="p3">電機企業</option>
								<option ng-model="p3">國貿企業</option>
							</select> 商品明細:<input type="text" name="pname"
								value="總類數:{{user.p1}} - 總機數:{{user.p2}} - 夥伴:{{user.p3}}">

						</div>
						<script>
							var app = angular.module('myApp', []);
							app.controller('formCtrl', function($scope) {
								$scope.master = {
									p1 : "",
									p2 : "",
									p3 : ""
								};
								$scope.reset = function() {
									$scope.user = angular.copy($scope.master);
								};
								$scope.reset();
							});
						</script>
				</div>

				<!-- hidden, input memberID -->
				<%
					request.getRemoteAddr();

					Connection con = null;
					Statement stmt = null;
					ResultSet rs = null;
					String url = "jdbc:mysql://localhost:3306/jsp";
					String user = "root";
					String password = "";
					String driver = "com.mysql.jdbc.Driver";
					Class.forName(driver);
					con = DriverManager.getConnection(url, user, password);
					stmt = con.createStatement();

					String sql = "SELECT * FROM `members` WHERE `uname` = '" + session.getAttribute("userid") + "'";
					rs = stmt.executeQuery(sql);
					if (rs != null)
					{
						while (rs.next())
						{
							String memberid = rs.getString(1);
				%>
				<p hidden>
					MemberID<input class="form-control text-box single-line" id="name"
						name="pmemberid" type="text" value="<%=memberid%>">
				</p>

				<%
					}
					}
				%>
				<p hidden>
					ProductID<input type="text" name="pid" value="999"
						placeholder="Disabled input">
				</p>

				<p>
					TotalPrice<input type="text" name="pprice"
						value="<%=request.getParameter("getCartPrice")%>"
						placeholder="Disabled input">
				</p>

				<p hidden>
					Contract<input type="text" name="pcontract" value="企業合作"
						placeholder="Disabled input">
				</p>

				<p>
					收件地址 : <input class="form-control text-box single-line"
						id="Context" name="address" type=text placeholder="請輸入收件人地址"
						required />
				</p>

				<p>
					付款方式 : <select class="form-control" name="pay" required>
						<option>信用卡</option>
						<option>轉帳</option>
					</select>
				</p>
				<br> <input type="submit" class="btn btn-danger" value="確認送出">
			</fieldset>
	</form>

</body>
</html>
