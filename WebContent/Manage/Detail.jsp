<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>商品管理</title>
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- load navibar -->
	<jsp:include page="/navibar.jsp" />
	<div>
		<div class="jumbotron">
			<h1>
				<b><font color="#FFFFFF">&nbsp&nbsp&nbsp商品管理</font></b>
			</h1>
			<br>
			<p></p>
			<a href="/Fileupload" class="btn btn-primary btn-lg" class="button"><span
				class="glyphicon glyphicon-plus-sign"></span> 商品上架</a> <a
				href="/Manage/productmanage.jsp" class="btn btn-primary btn-lg"
				class="button"><span class="glyphicon glyphicon-plus-sign"></span>
				商品管理</a> <a href="/Manage/ordermanage.jsp"
				class="btn btn-primary btn-lg" class="button"><span
				class="glyphicon glyphicon-list-alt"></span> 訂單管理</a> <a
				href="/Manage/infoedit.jsp" class="btn btn-warning btn-lg"
				class="button"><span class="glyphicon glyphicon-plus-sign"></span>
				修改會員資料</a>
		</div>
	</div>

	<div class="text-center">
		<h1>
			<b>商品ID : <%=request.getParameter("pid")%></b>
		</h1>

	</div>

	<%
		String pid = request.getParameter("pid");
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		String url = "jdbc:mysql://localhost:3306/jsp";
		String user = "root";
		String password = "";
		String driver = "com.mysql.jdbc.Driver";
		Class.forName(driver);
		con = DriverManager.getConnection(url, user, password);
		stmt = con.createStatement();
		String sql = "SELECT * FROM `product` WHERE id = '" + pid + "'";
		rs = stmt.executeQuery(sql);

		if (rs != null)
		{
			while (rs.next())
			{
				String id = rs.getString(1);
				String name = rs.getString(2);
				String price = rs.getString(3);
				String contract = rs.getString(4);
				String imgpath = rs.getString(5);
				String brand = rs.getString(7);
				String stock = rs.getString(8);
	%>
	<div class="text-center">
		<hr />
		<tr>
			<td><form action="Edit.jsp" method="post">
					<!-- load img -->
					<p>
						<img alt="" src="/data/<%=imgpath%>"
							style="width: 304px; height: 228px;">
					</p>
					<!-- hidden productID -->
					<div class="text-center" style="visibility: hidden">
						<label class="control-label col-md-2">Productid</label>
						<div class="col-md-10">
							<input class="form-control text-box single-line" id="pid"
								name="pid" type="text" value="<%=request.getParameter("pid")%>">
							<span class="field-validation-valid text-danger"
								data-valmsg-for="SalePrice" data-valmsg-replace="true"></span>
						</div>
					</div>

					<!-- productName -->
					<div class="form-group">
						<label class="control-label col-md-2">Name</label>
						<div class="col-md-10">
							<input class="form-control text-box single-line" id="name"
								name="name" type="text" value="<%=name%>"> <span
								class="field-validation-valid text-danger"
								data-valmsg-for="SalePrice" data-valmsg-replace="true"></span>
						</div>
					</div>

					<!-- productName -->
					<div class="form-group">
						<label class="control-label col-md-2">Brand</label>
						<div class="col-md-10">
							<input class="form-control text-box single-line" id="brand"
								name="brand" type="text" value="<%=brand%>"> <span
								class="field-validation-valid text-danger"
								data-valmsg-for="SalePrice" data-valmsg-replace="true"></span>
						</div>
					</div>

					<!-- productPrice -->
					<div class="form-group">
						<label class="control-label col-md-2">Price</label>
						<div class="col-md-10">
							<input class="form-control text-box single-line" id="price"
								name="price" type="text" value="<%=price%>"> <span
								class="field-validation-valid text-danger"
								data-valmsg-for="SalePrice" data-valmsg-replace="true"></span>
						</div>
					</div>
					<!-- productContract -->
					<div class="form-group">
						<label class="control-label col-md-2">Contract</label>
						<div class="col-md-10">
							<input class="form-control text-box single-line" id="contract"
								name="contract" type="text" value="<%=contract%>"> <span
								class="field-validation-valid text-danger"
								data-valmsg-for="SalePrice" data-valmsg-replace="true"></span>
						</div>
					</div>
					<!-- productStock -->
					<div class="form-group">
						<label class="control-label col-md-2">Stock</label>
						<div class="col-md-10">
							<input class="form-control text-box single-line" id="stock"
								name="stock" type="text" value="<%=stock%>"> <span
								class="field-validation-valid text-danger"
								data-valmsg-for="SalePrice" data-valmsg-replace="true"></span>
						</div>
					</div>
					<div class="form-group" style="visibility: hidden">
						<label class="control-label col-md-2">MemberID</label>
						<%
							sql = "SELECT * FROM `members` WHERE `uname` = '" + session.getAttribute("userid") + "'";
									rs = stmt.executeQuery(sql);
									if (rs != null)
									{
										while (rs.next())
										{
											String memid = rs.getString(1);
						%><div class="col-md-10">
							<input class="form-control text-box single-line" id="name"
								name="memberid" type="text" value="<%=memid%>"> <span
								class="field-validation-valid text-danger"
								data-valmsg-for="SalePrice" data-valmsg-replace="true"></span>
						</div>
						<%
							}
									}
						%>
					</div>
					<%
						}
						}
					%>

					<!-- submit -->
					<input type="submit" class="btn btn-danger" value="確認修改">

				</form></td>
		</tr>
		<form action="delete.jsp" method="post">
			<!-- hidden productID -->
			<div class="text-center" style="visibility: hidden">
				<label class="control-label col-md-2">Productid</label>
				<div class="col-md-10">
					<input class="form-control text-box single-line" id="pid"
						name="pid" type="text" value="<%=request.getParameter("pid")%>">
					<span class="field-validation-valid text-danger"
						data-valmsg-for="SalePrice" data-valmsg-replace="true"></span>
				</div>
			</div>
			<!-- submit -->
			<input type="submit" class="btn btn-danger" value="刪除商品">
		</form>
	</div>
</body>
</html>