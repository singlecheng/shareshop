<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%
	if ((session.getAttribute("userid") == null) || (session.getAttribute("userid") == ""))
	{
%>
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
<!-- load navibar -->
<jsp:include page="/navibar.jsp" />
<div align="center">
	Access denied <br /> <a href="/Account">Please Login</a>
</div>
<%
	} else
	{
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- load navibar -->
	<jsp:include page="/navibar.jsp" />
	<div>
		<div class="jumbotron">
			<h1><b><font color ="#FFFFFF">&nbsp&nbsp&nbsp商品管理</font></b></h1><br>
			<p></p>
			<a href="/Fileupload" class="btn btn-primary btn-lg" class="button"><span class="glyphicon glyphicon-plus-sign"></span> 商品上架</a>
			<a href="/Manage/productmanage.jsp" class="btn btn-primary btn-lg" class="button"><span class="glyphicon glyphicon-plus-sign"></span> 商品管理</a>
			<a href="/Manage/ordermanage.jsp" class="btn btn-primary btn-lg" class="button"><span class="glyphicon glyphicon-list-alt"></span> 訂單管理</a>
			<a href="/Manage/infoedit.jsp" class="btn btn-warning btn-lg" class="button"><span class="glyphicon glyphicon-plus-sign"></span> 修改會員資料</a>		
		</div>
	</div>

		<div class="container panel-warning">
			<div class="row">
				<%
					Connection con = null;
						Statement stmt = null;
						ResultSet rs = null;
						String url = "jdbc:mysql://localhost:3306/jsp";
						String user = "root";
						String password = "";
						String driver = "com.mysql.jdbc.Driver";
						Class.forName(driver);
						con = DriverManager.getConnection(url, user, password);
						stmt = con.createStatement();
						String sql = "SELECT * FROM `product` ORDER BY `product`.`time` DESC";
						rs = stmt.executeQuery(sql);

						if (rs != null)
						{
							while (rs.next())
							{
								String id = rs.getString(1);
								String name = rs.getString(2);
								String price = rs.getString(3);
								String contract = rs.getString(4);
								String imgpath = rs.getString(5);
								String time = rs.getString(6);
								String brand = rs.getString(7);
								String stock = rs.getString(8);
				%>
				<div class="col-md-3">
					<hr />
					<tr>
						<td><form action="Detail.jsp" method="GET">

								<p>
									<img alt="" src="/data/<%=imgpath%>"
										style="width: 304px; height: 228px;">
								</p>
								<p>
									ID :
									<%=id%></br>
								</p>
								<p>
									商品名稱 :
									<%=name%></br>
								</p>
								<p>
									價 格 :
									<%=price%></br>
								</p>
								<p>
									合 約 :
									<%=contract%></br>
								</p>
								<p>
									品 牌 :
									<%=brand%></br>
								</p>
								<p>
									庫存數量 :
									<%=stock%></br>
								</p>
								<p>
									上架時間 :
									<%=time%></br>
								</p>
								<p>
								<p hidden>
									Last Name: <input type="text" name="pid" value="<%=id%>" />
								</p>
								<input type="submit" class="btn btn-warning" value="商品修改">
							</form></td>
					</tr>
					<hr />
				</div>
				<%
					}
						}
				%>
			</div>
		</div>
	</div>

</body>
</html>

<%
	}
%>