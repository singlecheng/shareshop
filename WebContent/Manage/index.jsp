<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%
	if ((session.getAttribute("userid") == null) || (session.getAttribute("userid") == ""))
	{
%>
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
<!-- load navibar -->
<jsp:include page="/navibar.jsp" />

<div align="center">
	Access denied <br /> <a href="/Account">Please Login</a>
</div>
<%
	} else
	{
		//Get account ID
		request.getRemoteAddr();
		request.setCharacterEncoding("UTF-8");

		int accountid = 0;
		Statement stmt = null;
		Connection con = null;
		ResultSet rs = null;
		String url = "jdbc:mysql://localhost:3306/jsp";
		String user = "root";
		String password = "";
		String driver = "com.mysql.jdbc.Driver";
		Class.forName(driver);
		con = DriverManager.getConnection(url, user, password);
		stmt = con.createStatement();
		String sql = "SELECT * FROM `members` WHERE `uname` = '" + session.getAttribute("userid") + "'";
		rs = stmt.executeQuery(sql);
		if (rs != null)
		{
			while (rs.next())
			{
				accountid = rs.getInt(1);
			}
		}
		//account user admin id = 2
		if (accountid == 2)
		{
%><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- load navibar -->
	<jsp:include page="/navibar.jsp" />
	<div>
		<div class="jumbotron">
			<h1><b><font color ="#FFFFFF">&nbsp&nbsp&nbsp後台管理</font></b></h1><br>
			<p></p>
			<a href="/Fileupload" class="btn btn-primary btn-lg" class="button"><span class="glyphicon glyphicon-plus-sign"></span> 商品上架</a>
			<a href="/Manage/productmanage.jsp" class="btn btn-primary btn-lg" class="button"><span class="glyphicon glyphicon-plus-sign"></span> 商品管理</a>
			<a href="/Manage/ordermanage.jsp" class="btn btn-primary btn-lg" class="button"><span class="glyphicon glyphicon-list-alt"></span> 訂單管理</a>
			<a href="/Manage/infoedit.jsp" class="btn btn-warning btn-lg" class="button"><span class="glyphicon glyphicon-plus-sign"></span> 修改會員資料</a>		
		</div>
	</div>

</body>
</html>
<%
	} else
		{
			response.sendRedirect("/Manage/infoedit.jsp");
		}
	}
%>
