<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%
	if ((session.getAttribute("userid") == null) || (session.getAttribute("userid") == ""))
	{
%>
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
<!-- load navibar -->
<jsp:include page="/navibar.jsp" />
<div align="center">
	Access denied <br /> <a href="/Account">Please Login</a>
</div>
<%
	} else
	{
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- load navibar -->
	<jsp:include page="/navibar.jsp" />
	<div>
		<div class="jumbotron">
			<h1><b><font color ="#FFFFFF">&nbsp&nbsp&nbsp訂單管理</font></b></h1><br>
			<p></p>
			<a href="/Fileupload" class="btn btn-primary btn-lg" class="button"><span class="glyphicon glyphicon-plus-sign"></span> 商品上架</a>
			<a href="/Manage/productmanage.jsp" class="btn btn-primary btn-lg" class="button"><span class="glyphicon glyphicon-plus-sign"></span> 商品管理</a>
			<a href="/Manage/ordermanage.jsp" class="btn btn-primary btn-lg" class="button"><span class="glyphicon glyphicon-list-alt"></span> 訂單管理</a>
			<a href="/Manage/infoedit.jsp" class="btn btn-warning btn-lg" class="button"><span class="glyphicon glyphicon-plus-sign"></span> 修改會員資料</a>		
		</div>
	</div>

		<div class="container">
			<h2>訂單資料</h2>
			<table class="table">
				<thead>
					<tr>
						<th>訂單編號</th>
						<th>會員ID</th>
						<th>商品ID</th>
						<th>商品名稱</th>
						<th>購買數量</th>
						<th>總金額</th>
						<th>合約期限</th>
						<th>付款方式</th>
						<th>訂單成立時間</th>
						<th>是否結帳</th>
						<th>訂單確認</th>
					</tr>
				</thead>
				<%
					//Get account ID
						request.getRemoteAddr();
						request.setCharacterEncoding("UTF-8");

						int accountid = 0;
						Statement stmt = null;
						Connection con = null;
						ResultSet rs = null;
						String url = "jdbc:mysql://localhost:3306/jsp";
						String user = "root";
						String password = "";
						String driver = "com.mysql.jdbc.Driver";
						Class.forName(driver);
						con = DriverManager.getConnection(url, user, password);
						stmt = con.createStatement();
						String sql = "SELECT * FROM `members` WHERE `uname` = '" + session.getAttribute("userid") + "'";
						rs = stmt.executeQuery(sql);
						if (rs != null)
						{
							while (rs.next())
							{
								accountid = rs.getInt(1);
							}
						}
				%>
				<tbody>
					<%
						//Get order ID

							sql = "SELECT * FROM `orderlist` ORDER BY `time` DESC";
							rs = stmt.executeQuery(sql);

							if (rs != null)
							{
								//account user admin id = 2
								if (accountid == 2)
								{
									while (rs.next())
									{
										String id = rs.getString(1);
										String memberid = rs.getString(2);
										String pid = rs.getString(3);
										String pname = rs.getString(4);
										String quantity = rs.getString(5);
										String price = rs.getString(6);
										String contract = rs.getString(7);
										String pay = rs.getString(8);
										String time = rs.getString(9);
										String perchase = rs.getString(10);
					%>
					<tr>

						<td><%=id%></td>
						<td><%=memberid%></td>
						<td><%=pid%></td>
						<td><%=pname%></td>
						<td><%=quantity%></td>
						<td><%=price%></td>
						<td><%=contract%></td>
						<td><%=pay%></td>
						<td><%=time%></td>
						<td><%=perchase%></td>
						<td><form method="get" action="orderdetail.jsp">
								<p hidden>
									<input type="text" name="orderid" value="<%=id%>">
								</p>
								<input type="submit" class="btn btn-info" value="編輯"></td>
						</form>
					</tr>
					<%
						}
								}
							}
					%>
				</tbody>

			</table>
		</div>

	</div>
</body>
</html>
<%
	}
%>