<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*, java.util.*"%>

<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="/BeaconOrderServer/assets/image/favicon.ico" rel="SHORTCUT ICON">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
<script src="/Assets/js/jquery.min.js"></script>
<script src="/Assets/js/bootstrap.min.js"></script>

<title>Home - ShareShop</title>
<!-- Carousel Plugin -->
<style>
.carousel-inner>.item>img, .carousel-inner>.item>a>img {
	margin: auto;
}

.carousel {
	width: 100%;
	margin: auto;
}

.carousel-control.right, .carousel-control.left {
	background-image: none;
	color: #1abc9c;
}

.carousel-indicators li {
	border-color: #1abc9c;
}

.carousel-indicators li.active {
	background-color: #1abc9c;
}
</style>

</head>
<body style = background-color:#F0F0F0>
	<!-- load navibar -->
	<jsp:include page="/navibar.jsp" /><br>

	<div class="container text-center" style="background-color:#FFFFFF">
		<div class="container" style="background-color:#00AEAE">
			<h2>
				<font color="#D9FFFF"> <b>|&nbsp&nbsp&nbspNew 最新上架&nbsp&nbsp&nbsp|</b> </font>
			</h2><br>
		</div><br><br><br>
		<div id="myCarousel" class="carousel slide row" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
				<li data-target="#myCarousel" data-slide-to="3"></li>
				<li data-target="#myCarousel" data-slide-to="4"></li>
				<li data-target="#myCarousel" data-slide-to="5"></li>
				<li data-target="#myCarousel" data-slide-to="6"></li>
				<li data-target="#myCarousel" data-slide-to="7"></li>
				<li data-target="#myCarousel" data-slide-to="8"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="/Assets/image/a1.jpg" width="460" height="345">
					<div class="carousel-caption"></div>
				</div>
				<div class="item">
					<img src="/Assets/image/a2.jpg" width="460" height="345">
					<div class="carousel-caption"></div>
				</div>
				<div class="item">
					<img src="/Assets/image/a3.jpg" width="460" height="345">
					<div class="carousel-caption"></div>
				</div>
				<div class="item">
					<img src="/Assets/image/a4.jpg" width="460" height="345">
					<div class="carousel-caption"></div>
				</div>
				<div class="item">
					<img src="/Assets/image/a5.jpg" width="460" height="345">
					<div class="carousel-caption"></div>
				</div>
				<div class="item">
					<img src="/Assets/image/a6.jpg" width="460" height="345">
					<div class="carousel-caption"></div>
				</div>
				<div class="item">
					<img src="/Assets/image/a7.jpg" width="460" height="345">
					<div class="carousel-caption"></div>
				</div>
				<div class="item">
					<img src="/Assets/image/a8.jpg" width="460" height="345">
					<div class="carousel-caption"></div>
				</div>
				<div class="item">
					<img src="/Assets/image/a9.jpg" width="460" height="345">
					<div class="carousel-caption"></div>
				</div>
			</div>
			<!-- Left and right controls -->
			<a class="left carousel-control" href="//#myCarousel" role="button"
				data-slide="prev"> <span
				class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a> <a class="right carousel-control" href="#myCarousel" role="button"
				data-slide="next"> <span
				class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div><br><br><br>
	</div>

	<div class="container text-center" style="background-color:#FFFFFF">
		<div class="container" style="background-color:#00AEAE">
			<h2>
				<span class="glyphicon glyphicon-star" style='color:#D9FFFF;'></span><font color="#D9FFFF"> <b>精選商品</b> </font><span class="glyphicon glyphicon-star" style='color:#D9FFFF;'></span>
			</h2><br>
		</div><br>

		<div class="row">
			<%
				Connection con = null;
				Statement stmt = null;
				ResultSet rs = null;
				String url = "jdbc:mysql://localhost:3306/jsp";
				String user = "root";
				String password = "";
				String driver = "com.mysql.jdbc.Driver";
				Class.forName(driver);
				con = DriverManager.getConnection(url, user, password);
				stmt = con.createStatement();
				String sql = "SELECT * FROM `product` ORDER BY `product`.`time` DESC";
				rs = stmt.executeQuery(sql);

				if (rs != null)
				{
					while (rs.next())
					{
						String id = rs.getString(1);
						String name = rs.getString(2);
						String price = rs.getString(3);
						String contract = rs.getString(4);
						String imgpath = rs.getString(5);
						String time = rs.getString(6);
						String brand = rs.getString(7);
						String stock = rs.getString(8);
			%>
			<div class="col-md-3">
				<hr />
				<tr>
					<td><form action="Detail.jsp" method="GET">
							<p>
								<img alt="" src="/data/<%=imgpath%>"
									style="width: 280px; height: 200px;">
							</p>
							<p align="left">
								商品名稱 :
								<%=name%></br>
							</p>
							<p align="left">
								價 格 :
								<%=price%></br>
							</p>
							<p align="left">
								合 約 :
								<%=contract%></br>
							</p>
							<p align="left">
								品 牌 :
								<%=brand%></br>
							</p>
							<p align="left">
								庫存數量 :
								<%=stock%></br>
							</p>
							<p align="left">
								上架時間 :
								<%=time%></br>
							</p>
							<p>
							<p hidden>
								ProductID: <input type="text" name="pid" value="<%=id%>" />
							</p>
							<input type="submit" class="btn btn-success" value="直接結帳">
						</form>
						</p></td>
				</tr>
				<hr />
			</div>
			<%
				}
				}
			%>
		</div>
	</div>
	
	<br><footer>
						<ul class="actions">
							<li>
								<a href="#" class="button alt big"><center>TOP</center></a>
							</li>
						</ul>
						</footer>

	<hr>
	<footer class="container-fluid bg-4 text-center" style="background-color:#00CACA">
		<p><br>
			<font color ="#FFFFFF">Powered By : </font><a href="https://www.android.com/">Android</a> | <a
				href="https://angularjs.org/">AngularJS</a> | <a
				href="https://bitbucket.org/">Bitbucket</a> | <a
				href="http://getbootstrap.com/">Bootstrap</a> | <a
				href="http://codeigniter.org.tw/">CodeIgniter</a> | <a
				href="https://eclipse.org/downloads/">Eclipse</a> | <a
				href="http://estimote.com/">Estimote</a> | <a
				href="https://github.com/">GitHub</a> | <a
				href="https://developer.apple.com/ibeacon/">iBeacon</a> | <a
				href="https://jquery.com/">jQuery</a> | <a
				href="http://www.json.org/">JSON</a> | <a
				href="https://www.mysql.com/downloads/">MySQL</a> | <a
				href="http://php.net/downloads.php">PHP</a> | <a
				href="https://www.sourcetreeapp.com/">SourceTree</a> | <a
				href="https://developer.apple.com/swift/">Swift</a> | <a
				href="https://www.apachefriends.org/zh_tw/index.html">Xampp</a> |
		</p>
		<p class="footer_copyright">
			<font color ="#FFFFFF">© 2016 </font><a href="mailto:singleqiang@gmail.com">Single Studio</a>. 
			<font color ="#FFFFFF">All Rights Reserved.</font>
		</p>

		<!-- 訪客計數器 -->
		<%
			Integer hitsCount = (Integer) application.getAttribute("hitCounter");
			if (hitsCount == null || hitsCount == 0)
			{
				/* First visit */
				out.println("Welcome !");
				hitsCount = 1;
			} else
			{
				/* return visit */
				out.println("Welcome back !");
				/*hitsCount += 1;*/
			}
			application.setAttribute("hitCounter", hitsCount);
		%>

		<p><font color ="#FFFFFF">
			累計拜訪人數 : 
			<%=hitsCount%> 人</font></p>

	</footer>
</body>
</html>