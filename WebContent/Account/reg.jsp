<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>註冊</title>
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body style=background-image:url('/Assets/image/g2.png');background-repeat:no-repeat;background-size:cover;width: 100%;>


	<!-- load navibar -->
	<jsp:include page="/navibar.jsp" /><br>
	<form method="post" action="registration.jsp">

<div class="container">
    <div class="col-md-offset-4 col-md-4">
        <h2 class="text-center"><strong>會員註冊</strong></h2>
        <form ="form-horizontal" role="form">
       <font color="red">*</font> 為必填欄位。
            <div class="form-group has-primary">		
                <label for="fname"><strong>First Name：</strong></label>
                <input type="text" class="form-control" name="fname" placeholder="請輸入您的名字">
            </div>
            <div class="form-group has-primary">
                <label for="lname"><strong>Last Name：</strong></label>
                <input type="text" class="form-control" name="lname" placeholder="請輸入您的姓氏">
            </div>
        
            <div class="form-group has-primary">		
                &nbsp<span class="glyphicon glyphicon-user"></span>&nbsp&nbsp<label for="uname"><strong>帳&nbsp&nbsp號 <font color="red">*</font>：</strong></label>
                <input type="text" class="form-control" name="uname" required placeholder="請輸入使用者帳號">
            </div>
            <div class="form-group has-primary">
                &nbsp<span class="glyphicon glyphicon-lock"></span>&nbsp&nbsp<label for="pass"><strong>密&nbsp&nbsp碼 <font color="red">*</font>：</strong></label>
                <input type="password" class="form-control" name="pass" required placeholder="請輸入使用者密碼">
            </div>
                        <div class="form-group has-primary">		
                &nbsp<span class="glyphicon glyphicon-envelope"></span>&nbsp&nbsp<label for="email"><strong>電子郵件 <font color="red">*</font>：</strong></label>
                <input type="email" class="form-control" name="email" required placeholder="請輸入電子郵件">
            </div>
<br>
            <center><button type="submit" class="btn btn-primary">&nbsp&nbsp&nbsp送&nbsp&nbsp出&nbsp&nbsp&nbsp</button>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            <button type="reset" class="btn btn-default">&nbsp&nbsp&nbsp清&nbsp&nbsp除&nbsp&nbsp&nbsp</button></center><br><br><br><br><br>
        </form>
    </div>
</div>	
		
			<center><strong>已經註冊成為會員。</strong><button type="submit" class="btn-link"><strong><a href="index.jsp">點此登入</a></strong></button></center>
	</form><br><br>
	
		<footer class="container-fluid bg-4 text-center" style="background-color:#00CACA">
		<p><br>
			<font color ="#FFFFFF">Powered By : </font><a href="https://www.android.com/">Android</a> | <a
				href="https://angularjs.org/">AngularJS</a> | <a
				href="https://bitbucket.org/">Bitbucket</a> | <a
				href="http://getbootstrap.com/">Bootstrap</a> | <a
				href="http://codeigniter.org.tw/">CodeIgniter</a> | <a
				href="https://eclipse.org/downloads/">Eclipse</a> | <a
				href="http://estimote.com/">Estimote</a> | <a
				href="https://github.com/">GitHub</a> | <a
				href="https://developer.apple.com/ibeacon/">iBeacon</a> | <a
				href="https://jquery.com/">jQuery</a> | <a
				href="http://www.json.org/">JSON</a> | <a
				href="https://www.mysql.com/downloads/">MySQL</a> | <a
				href="http://php.net/downloads.php">PHP</a> | <a
				href="https://www.sourcetreeapp.com/">SourceTree</a> | <a
				href="https://developer.apple.com/swift/">Swift</a> | <a
				href="https://www.apachefriends.org/zh_tw/index.html">Xampp</a> |
		</p>
		<p class="footer_copyright">
			<font color ="#FFFFFF">© 2016 </font><a href="mailto:singleqiang@gmail.com">Single Studio</a>. 
			<font color ="#FFFFFF">All Rights Reserved.</font>
		</p>

		<!-- 訪客計數器 -->
		<%
			Integer hitsCount = (Integer) application.getAttribute("hitCounter");
			if (hitsCount == null || hitsCount == 0)
			{
				/* First visit */
				out.println("Welcome !");
				hitsCount = 1;
			} else
			{
				/* return visit */
				out.println("Welcome back !");
				//hitsCount += 1;
			}
			application.setAttribute("hitCounter", hitsCount);
		%>

		<p><font color ="#FFFFFF">
			累計拜訪人數 : 
			<%=hitsCount%> 人</font></p>

	</footer>
	
</body>
</html>