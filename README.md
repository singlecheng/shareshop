# README #

### CYCU-IM 104-1 JSP網路程式設計 期末專案 ###

### Summary of set up Xampp ###

### File set ###

--------------------------------------------------------------------

server.xml

<Context path="" docBase="C:\xampp\tomcat\webapps\ROOT\WebContent" debug="0" crosscontext="true" reloadable="true">

<Manager className="org.apache.catalina.session.PersistentManager" saveOnRestart="true">

<Store className="org.apache.catalina.session.FileStore"/>

</Manager>

</Context>

--------------------------------------------------------------------
web.xml

<context-param> 

<description>Location to store uploaded file</description> 

<param-name>file-upload</param-name> 

<param-value>

C:\xampp\tomcat\webapps\ROOT\WebContent\data\

</param-value> 

</context-param>

--------------------------------------------------------------------

httpd-xampp.conf

<LocationMatch "^/(?i:(?:xampp|licenses|phpmyadmin|webalizer|server-status|server-info))">

Order deny,allow

Deny from all

Allow from all

ErrorDocument 403 /error/HTTP_XAMPP_FORBIDDEN.html.var

</LocationMatch>

--------------------------------------------------------------------

### Import file to lib 

-fileupload

commons-fileupload-1.3.jar

commons-io-2.4.jar

-sql

mysql-connector-java-5.1.38-bin.jar

-JSON

json_simple-1.1.jar

org.json-20120521.jar