-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生時間： 2016-01-16 14:28:22
-- 伺服器版本: 10.1.9-MariaDB
-- PHP 版本： 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `jsp`
--

-- --------------------------------------------------------

--
-- 資料表結構 `guestbook`
--

CREATE TABLE `guestbook` (
  `no` tinyint(4) NOT NULL,
  `name` varchar(10) DEFAULT NULL,
  `mail` varchar(30) DEFAULT NULL,
  `subject` varchar(30) DEFAULT NULL,
  `content` text,
  `putdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `guestbook`
--

INSERT INTO `guestbook` (`no`, `name`, `mail`, `subject`, `content`, `putdate`, `ip`) VALUES
(1, '草泥馬', 'fuckyou@com.tw', 'WTF', '好像很厲害？', '2016-01-10 16:00:00', '111.70.6.163'),
(2, 'c86', 'kok', 'kpokopk', 'opkopkop', '2016-01-14 16:00:00', '140.135.113.242'),
(3, 'jooi', 'jioji', 'oi', 'i', '2016-01-14 16:00:00', '140.135.113.242'),
(4, 'jj', 'joij', 'oiji', 'ji', '2016-01-14 16:00:00', '140.135.113.242'),
(5, 'jjioj', 'oijijiij', 'oijijojioj', 'tftft', '2016-01-14 16:00:00', '140.135.113.242'),
(6, '', '', '下一頁', '每五筆資料算一頁', '2016-01-14 16:00:00', '140.135.113.242'),
(7, '訪客名稱', '123@gmail.com', '測試', '每五比會跳下一頁', '2016-01-14 16:00:00', '114.34.40.113'),
(8, '訪客來了', '', '這是一段很重要的留言', '今天天氣應該不錯\r\n吧\r\n希望不要下雨', '2016-01-14 16:00:00', '114.34.40.113');

-- --------------------------------------------------------

--
-- 資料表結構 `members`
--

CREATE TABLE `members` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `last_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `email` varchar(45) CHARACTER SET latin1 NOT NULL,
  `uname` varchar(45) CHARACTER SET latin1 NOT NULL,
  `pass` varchar(45) CHARACTER SET latin1 NOT NULL,
  `regdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `members`
--

INSERT INTO `members` (`id`, `first_name`, `last_name`, `email`, `uname`, `pass`, `regdate`) VALUES
(1, 'single', 'single', 'single@com.tw', 'single', '123', '2015-12-29'),
(2, 'First', 'Last', 'admin@com.tw', 'admin', '123', '2016-01-02'),
(3, '', '', '', '123', '', '2016-01-15'),
(4, 'a', 'a', 'aa@com.tw', 'aaa', 'aaa', '2016-01-15'),
(5, 'Ã¥?Â¨Ã¥?Â¨Ã¥?Â¨', 'Ã¥?Â¨', '1111@yahoo.com.tw', '1', '1', '2016-01-15'),
(6, '123', '123', '123@com.tw', '123', '123', '2016-01-15');

-- --------------------------------------------------------

--
-- 資料表結構 `orderlist`
--

CREATE TABLE `orderlist` (
  `id` int(11) NOT NULL,
  `memberid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `pname` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` varchar(50) NOT NULL,
  `contract` varchar(50) NOT NULL,
  `pay` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `perchase` int(11) NOT NULL DEFAULT '0',
  `address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '預設商品',
  `price` varchar(50) NOT NULL DEFAULT '299',
  `contract` varchar(50) NOT NULL DEFAULT '1',
  `imgpath` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `brand` varchar(50) NOT NULL DEFAULT 'hTC',
  `stock` varchar(50) NOT NULL DEFAULT '5',
  `memberid` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `contract`, `imgpath`, `time`, `brand`, `stock`, `memberid`) VALUES
(1, 'One A9 64GB', '11900', '1年', 'a1.jpg', '2016-01-10 15:19:58', 'hTC', '0', '2'),
(2, 'Desire 727 32GB', '9900', '1年', 'a2.jpg', '2016-01-10 15:19:58', 'hTC', '22', '2'),
(4, 'One M9 64GB (香檳金)', '18800', '1年', 'a4.jpg', '2016-01-10 15:20:20', 'hTC', '3', '2'),
(5, 'Desire Eye 16GB (紅)', '10999', '1年', 'a5.jpg', '2016-01-10 15:20:49', 'hTC', '9', '2'),
(6, 'One M8 32GB (深藍)', '9900', '1年', 'a6.jpg', '2016-01-10 15:20:49', 'hTC', '2', '2'),
(7, 'One M9+ 64GB (香檳金)', '18900', '1年', 'a7.jpg', '2016-01-10 15:21:08', 'hTC', '36', '2'),
(8, 'Desire Eye 16GB (白)', '10999', '1年', 'a8.jpg', '2016-01-10 15:21:08', 'hTC', '53', '2'),
(12, 'iPhone 6S 64GB (玫瑰金)', '18800', '1年', 'i6plus.jpg', '2016-01-14 18:49:31', 'Apple', '6', '2'),
(13, 'iPhone 5S 16GB (太空灰)', '12900', '1年', 'i5s.jpg', '2016-01-14 21:22:11', 'Apple', '10', '2'),
(14, 'Xperia Z5 32GB (黑)', '14900', '1年', 'Sony_Xperia_Z5_0902150402705_360x270.jpg', '2016-01-15 00:06:21', 'Sony', '2', '2'),
(15, 'GALAXY Note 5 32GB (金)', '17900', '1年', 'SAMSUNG_GALAXY_Note_5_32GB_0813181713710_360x270.jpg', '2016-01-15 00:07:32', 'Samsung', '14', '2'),
(999, '企業商品(勿點)', '9999999', '1', 'enterprice.jpg', '2016-01-09 00:27:53', '企業商品', '9999990', '');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `guestbook`
--
ALTER TABLE `guestbook`
  ADD PRIMARY KEY (`no`);

--
-- 資料表索引 `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `orderlist`
--
ALTER TABLE `orderlist`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `guestbook`
--
ALTER TABLE `guestbook`
  MODIFY `no` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- 使用資料表 AUTO_INCREMENT `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- 使用資料表 AUTO_INCREMENT `orderlist`
--
ALTER TABLE `orderlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- 使用資料表 AUTO_INCREMENT `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1002;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
